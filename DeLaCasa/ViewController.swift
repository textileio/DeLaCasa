import UIKit
import RxSwift
import RxDataSources
import RxRealm
import RealmSwift

struct RemindersSection {
  var header: String
  var reminders: [Item]
  let timing: Reminder.Timing

  init(header: String, reminders: [Item], timing: Reminder.Timing) {
    self.header = header
    self.reminders = reminders
    self.timing = timing
  }
}

extension RemindersSection: AnimatableSectionModelType {
  typealias Item = Reminder
  typealias Identity = String

  var identity: String {
    return header
  }

  var items: [Reminder] {
    return reminders
  }

  init(original: RemindersSection, items: [Item]) {
    self = original
    self.reminders = items
  }
}

extension Reminder: IdentifiableType {
  typealias Identity = String

  var identity: String {
    return id
  }
}

class ViewController: UIViewController {

  @IBOutlet weak var tableView: UITableView!

  let notifier = Notifier()

  private let disposeBag = DisposeBag()

  override func viewDidLoad() {
    super.viewDidLoad()

    title = "DeLaCasa Reminders"

    let dataSource = RxTableViewSectionedAnimatedDataSource<RemindersSection>()

    dataSource.configureCell = { dataSource, tableView, indexPath, reminder -> UITableViewCell in
      let cell = tableView.dequeueReusableCell(withIdentifier: "reminderCell", for: indexPath)
      cell.textLabel?.text = reminder.title
      return cell
    }

    dataSource.titleForHeaderInSection = { dataSource, section in
      return dataSource[section].header
    }

    dataSource.canEditRowAtIndexPath = { _ in
      return true
    }

    dataSource.canMoveRowAtIndexPath = { _ in
      return true
    }

    dataSource.animationConfiguration = AnimationConfiguration(insertAnimation: .top,
                                                               reloadAnimation: .fade,
                                                               deleteAnimation: .left)

    let realm = try! Realm()
    let reminders = realm.objects(Reminder.self)

    Observable.collection(from: reminders)
      .map { $0.filter { $0.state == .pending } }
      .map { $0.sorted { $0.priority < $1.priority } }
      .map { reminders -> [RemindersSection] in
        let onArrivalSection = RemindersSection(header: "After Arriving", reminders: reminders.filter { $0.timing == .onArrival }, timing: .onArrival)
        let onDepartureSection = RemindersSection(header: "After Departing", reminders: reminders.filter { $0.timing == .onDeparture }, timing: .onDeparture)
        let enRouteSection = RemindersSection(header: "When En Route", reminders: reminders.filter { $0.timing == .enRoute }, timing: .enRoute)
        var sections: [RemindersSection] = []
        // For some reason, this is causing un-animated insertion of items
        for section in [onArrivalSection, onDepartureSection, enRouteSection] {
          if section.items.count > 0 {
            sections.append(section)
          }
        }
        return sections
      }
      .bind(to: tableView.rx.items(dataSource: dataSource))
      .disposed(by: disposeBag)

    tableView.rx.itemDeleted.asObservable()
      .subscribe(onNext: { indexPath in
        try! realm.write {
          dataSource[indexPath.section].items[indexPath.row].state = .deleted
        }
        print("Deleted \(indexPath)")
      })
      .disposed(by: disposeBag)

    tableView.rx.itemMoved.asObservable()
      .subscribe(onNext: { [weak self] sourceIndexPath, destinationIndexPath in
        let destinationTiming: Reminder.Timing
        switch destinationIndexPath.section {
        case 0:
          destinationTiming = .onArrival
        case 1:
          destinationTiming = .onDeparture
        default:
          destinationTiming = .enRoute
        }
        try! realm.write {
          dataSource[destinationIndexPath.section].items[destinationIndexPath.row].timing = destinationTiming
        }
        self?.updatePriorities(dataSource: dataSource)

        print("Moved from \(sourceIndexPath) to \(destinationIndexPath)")
      })
      .disposed(by: disposeBag)

    let infoButton = UIButton(type: .infoLight)
    let infoItem = UIBarButtonItem(customView: infoButton)
    infoButton.rx.tap
      .bind { [weak self] in
        self?.performSegue(withIdentifier: "showAbout", sender: nil)
      }
      .disposed(by: disposeBag)

    let addItem = UIBarButtonItem(barButtonSystemItem: .add, target: nil, action: nil)
    addItem.rx.tap
      .bind { [weak self] in
        self?.presentReminderCreation(dataSource: dataSource)
      }
      .disposed(by: disposeBag)

    navigationItem.rightBarButtonItems = [addItem, infoItem]

    let editItem = UIBarButtonItem(barButtonSystemItem: .edit, target: nil, action: nil)
    let doneEditingItem = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: nil)

    navigationItem.leftBarButtonItem = editItem

    doneEditingItem.rx.tap
      .bind {
        self.tableView.setEditing(false, animated: true)
        self.navigationItem.setLeftBarButton(editItem, animated: true)
      }
      .disposed(by: disposeBag)

    editItem.rx.tap
      .bind {
        self.tableView.setEditing(true, animated: true)
        self.navigationItem.setLeftBarButton(doneEditingItem, animated: true)
      }
      .disposed(by: disposeBag)

    self.rx.sentMessage(#selector(viewDidAppear(_:)))
      .subscribe(onNext: { [weak self] _ in
        if dataSource.sectionModels.count == 0 {
          self?.presentReminderCreation(dataSource: dataSource)
        }
      })
      .disposed(by: disposeBag)

  }

  private func updatePriorities(dataSource: RxTableViewSectionedAnimatedDataSource<RemindersSection>) {
    let realm = try! Realm()
    try! realm.write {
      for section in dataSource.sectionModels {
        for (index, reminder) in section.items.enumerated() {
          reminder.priority = index
        }
      }
    }
  }

  private func presentReminderCreation(dataSource: RxTableViewSectionedAnimatedDataSource<RemindersSection>) {
    let alertController = UIAlertController(title: "New Reminder", message: nil, preferredStyle: .alert)

    alertController.addTextField { textField in
      textField.placeholder = "Remind me to"
      textField.autocapitalizationType = .sentences
    }

    let textField = (alertController.textFields?.first)!

    let action1 = UIAlertAction(title: "After arriving home", style: .default) { _ in
      guard let text = textField.text else { return }
      let priority = dataSource.sectionModels.filter { $0.timing == .onArrival }.first?.items.count ?? 0
      self.createReminder(withTilte: text, timing: .onArrival, andPriority: priority)
    }
    let action2 = UIAlertAction(title: "After departing home", style: .default) { _ in
      guard let text = textField.text else { return }
      let priority = dataSource.sectionModels.filter { $0.timing == .onDeparture }.first?.items.count ?? 0
      self.createReminder(withTilte: text, timing: .onDeparture, andPriority: priority)
    }
    let action3 = UIAlertAction(title: "When en route to home", style: .default) { _ in
      guard let text = textField.text else { return }
      let priority = dataSource.sectionModels.filter { $0.timing == .enRoute }.first?.items.count ?? 0
      self.createReminder(withTilte: text, timing: .enRoute, andPriority: priority)
    }
    let action4 = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)

    let titleIsValid: Observable<Bool> = textField.rx.text
      .map { text in
        guard let text = text else { return false }
        return text.characters.count > 0
    }

    titleIsValid.bind(to: action1.rx.isEnabled).disposed(by: disposeBag)
    titleIsValid.bind(to: action2.rx.isEnabled).disposed(by: disposeBag)
    titleIsValid.bind(to: action3.rx.isEnabled).disposed(by: disposeBag)

    alertController.addAction(action1)
    alertController.addAction(action2)
    alertController.addAction(action3)
    alertController.addAction(action4)

    self.present(alertController, animated: true, completion: nil)
  }

  private func createReminder(withTilte title: String, timing: Reminder.Timing, andPriority priority: Int) {
    let realm = try! Realm()
    let reminder = Reminder()
    reminder.title = title
    reminder.timing = timing
    reminder.priority = priority
    try! realm.write {
      realm.add(reminder)
    }
  }

}

