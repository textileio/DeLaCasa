import RealmSwift

class Reminder: Object {

  enum Timing: Int {
    case onArrival = 0
    case onDeparture = 1
    case enRoute = 2
  }

  enum State: Int {
    case pending = 0
    case complete = 1
    case deleted = 2
  }

  dynamic var id = UUID().uuidString
  dynamic var createdAt = Date()
  dynamic var title = ""
  dynamic var priority: Int = 0
  dynamic var timingValue = Timing.onArrival.rawValue
  dynamic var stateValue = State.pending.rawValue

  var timing: Timing {
    get {
      return Timing(rawValue: timingValue)!
    }
    set {
      timingValue = newValue.rawValue
    }
  }
  var state: State {
    get {
      return State(rawValue: stateValue)!
    }
    set {
      stateValue = newValue.rawValue
    }
  }

  override static func primaryKey() -> String? {
    return "id"
  }
}
