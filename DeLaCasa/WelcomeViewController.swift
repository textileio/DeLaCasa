//
//  WelcomeViewController.swift
//  DeLaCasa
//
//  Created by Aaron Sutula on 4/12/17.
//  Copyright © 2017 We Are Set. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import UserNotifications
import Set

class WelcomeViewController: UIViewController {

  @IBOutlet weak var notificationsButton: UIButton!
  @IBOutlet weak var locationButton: UIButton!
  @IBOutlet weak var motionButton: UIButton!

  private let disposeBag = DisposeBag()

  override func viewDidLoad() {
    super.viewDidLoad()

    title = "Welcome!"

    notificationsButton.isEnabled = false
    locationButton.isEnabled = false
    motionButton.isEnabled = false

    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
      self.notificationsButton.isEnabled = true
    }

    notificationsButton.rx.tap
      .bind {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) { granted, error in
          DispatchQueue.main.sync {
            self.notificationsButton.isEnabled = false
            self.locationButton.isEnabled = true
          }
        }
      }
      .addDisposableTo(disposeBag)

    locationButton.rx.tap
      .bind {
        self.locationButton.isEnabled = false
        self.motionButton.isEnabled = true
        SetSDK.instance.promtForLocationAccess()
      }
      .addDisposableTo(disposeBag)

    motionButton.rx.tap
      .bind {
        self.motionButton.isEnabled = false
        SetSDK.instance.promptForMotionAccess()
        self.performSegue(withIdentifier: "showReminders", sender: nil)
      }
      .addDisposableTo(disposeBag)
  }
}
