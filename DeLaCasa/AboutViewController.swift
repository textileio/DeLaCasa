import UIKit
import RxSwift

class AboutViewController: UIViewController {

  @IBOutlet weak var versionLabel: UILabel!
  @IBOutlet weak var setButton: UIButton!
  @IBOutlet weak var shareButton: UIButton!

  let disoseBag = DisposeBag()

  override func viewDidLoad() {
    title = "About"

    let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    let build = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    versionLabel.text = "Version \(version) (\(build))"

    setButton.rx.tap
      .bind {
        UIApplication.shared.open(URL(string: "<your URL>")!)
      }
      .disposed(by: disoseBag)

    shareButton.rx.tap
      .bind { [weak self] _ in
        let shareSheet = UIActivityViewController(activityItems: [URL(string: "<your app store URL>")!], applicationActivities: nil)
        self?.present(shareSheet, animated: true, completion: nil)
      }
      .disposed(by: disoseBag)

    let closeItem = UIBarButtonItem(title: "Close", style: .done, target: nil, action: nil)
    navigationItem.rightBarButtonItem = closeItem
    closeItem.rx.tap
      .bind { [weak self] _ in
        self?.dismiss(animated: true, completion: nil)
      }
      .disposed(by: disoseBag)
  }
}
