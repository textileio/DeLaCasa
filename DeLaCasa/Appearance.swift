//
//  Appearance.swift
//  DeLaCasa
//
//  Created by Aaron Sutula on 4/12/17.
//  Copyright © 2017 We Are Set. All rights reserved.
//

import Foundation
import UIKit

func applyAppearance() {
  let fontName = "HelveticaNeue-Light"
  let redColor = UIColor(red: 232/255, green: 104/255, blue: 92/255, alpha: 1.0)

  UILabel.appearance(whenContainedInInstancesOf: [ViewController.self]).fontName = fontName
  UILabel.appearance(whenContainedInInstancesOf: [UIButton.self]).fontName = fontName

  let navigationTitleFont = UIFont(name: fontName, size: 20)!
  let barButtonItemFont = UIFont(name: fontName, size: 18)!

  UINavigationBar.appearance().isTranslucent = false
  UINavigationBar.appearance().barStyle = UIBarStyle.black
  UINavigationBar.appearance().barTintColor = redColor
  UINavigationBar.appearance().backgroundColor = redColor
  UINavigationBar.appearance().tintColor = UIColor.white
  UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: navigationTitleFont]

  UIBarButtonItem.appearance().setTitleTextAttributes([NSFontAttributeName: barButtonItemFont], for: .normal)

  UILabel.appearance(whenContainedInInstancesOf: [UITableViewHeaderFooterView.self]).fontSize = 16

  UIButton.appearance(whenContainedInInstancesOf: [WelcomeViewController.self]).tintColor = redColor
  UIButton.appearance(whenContainedInInstancesOf: [AboutViewController.self]).tintColor = redColor
}

extension UILabel {
  var fontName: String {
    get {
      return self.font.fontName
    }
    set {
      self.font = UIFont(name: newValue, size: self.font.pointSize)
    }
  }

  var fontSize: CGFloat {
    get {
      return self.font.pointSize
    }
    set {
      self.font = UIFont(name: self.font.fontName, size: newValue)
    }
  }
}
