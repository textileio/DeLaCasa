import Set
import RealmSwift
import UserNotifications
import CoreLocation

enum ReminderAction: String {
  case clear = "clear"
  case snooze = "snooze"
}

enum ConfirmHomeAction: String {
  case yes = "yes"
  case no = "no"
}

class Notifier: NSObject {
  override init() {
    super.init()
    setupNotifications()
    launchSet()
  }
}

fileprivate let clientId = "<Your SetSDK Client ID>"
fileprivate let clientSecret = "<Your SetSDK Client Secret>"

fileprivate let userIdKey = "setUserId"
fileprivate let homePlaceIdKey = "homeId"
fileprivate let isHomeKey = "isHome"

fileprivate let reminderNotificationsCategoryId = "reminders"
fileprivate let confirmHomeNotificationsCategoryId = "confirmHome"

fileprivate let reminderIdUserInfoKey = "reminderId"
fileprivate let homePlaceIdUserInfoKey = "homePlaceId"

extension Notifier {

  fileprivate var realm: Realm {
    return try! Realm()
  }

  private var userId: String {
    if let userId = UserDefaults.standard.string(forKey: userIdKey) {
      return userId
    } else {
      let userId = UUID().uuidString
      UserDefaults.standard.set(userId, forKey: userIdKey)
      UserDefaults.standard.synchronize()
      return userId
    }
  }

  fileprivate func setupNotifications() {
    let center = UNUserNotificationCenter.current()
    center.delegate = self

    let snoozeAction = UNNotificationAction(identifier: ReminderAction.snooze.rawValue,
                                            title: "Next Time",
                                            options: [])
    let clearAction = UNNotificationAction(identifier: ReminderAction.clear.rawValue,
                                           title: "Got It",
                                           options: [])
    let remindersCategory = UNNotificationCategory(identifier: reminderNotificationsCategoryId,
                                                   actions: [snoozeAction, clearAction],
                                                   intentIdentifiers: [],
                                                   options: [.customDismissAction])

    let yesAction = UNNotificationAction(identifier: ConfirmHomeAction.yes.rawValue,
                                            title: "Yes",
                                            options: [])
    let noAction = UNNotificationAction(identifier: ConfirmHomeAction.no.rawValue,
                                           title: "No",
                                           options: [])
    let confirmHomeCategory = UNNotificationCategory(identifier: confirmHomeNotificationsCategoryId,
                                                   actions: [yesAction, noAction],
                                                   intentIdentifiers: [],
                                                   options: [.customDismissAction])

    center.setNotificationCategories([remindersCategory, confirmHomeCategory])
  }

  fileprivate func launchSet() {
    let config = Configuration(clientId: clientId, clientSecret: clientSecret, userId: userId, shareUserData: true)
    SetSDK.instance.launch(with: config) { possibleError in
      if let error = possibleError {
        print("Got an error launching SetSDK: \(error)")
        return
      }
      self.setupSubscriptions()
    }
  }

  private func setupSubscriptions() {
    SetSDK.instance.onArrival(to: .home) { event in
      self.notifyForTiming(.onArrival)
      self.notifyConfirmHome(id: event.id)
    }

    SetSDK.instance.onDeparture(from: .home) { event in
      self.notifyForTiming(.onDeparture)
    }

    SetSDK.instance.onDeparture(from: .any) { event in
      if let state = SetSDK.instance.getDestination(from: event.location)?.first,
        state.trip.end.label == "home" {
        self.notifyForTiming(.enRoute)
      }
    }

  }

  fileprivate func notifyForTiming(_ timing: Reminder.Timing) {
    let reminders: [Reminder] = realm.objects(Reminder.self).filter { $0.timing == timing && $0.state == .pending }
    reminders.forEach { self.notifyReminder($0) }
  }

  private func notifyReminder(_ reminder: Reminder) {
    let content = UNMutableNotificationContent()
    content.title = "Reminder"
    content.body = reminder.title
    content.sound = UNNotificationSound.default()
    content.userInfo = [reminderIdUserInfoKey: reminder.id]
    content.categoryIdentifier = reminderNotificationsCategoryId

    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.5, repeats: false)

    let request = UNNotificationRequest(identifier: "reminder_\(reminder.id)", content: content, trigger: trigger)

    let center = UNUserNotificationCenter.current()
    center.add(request) { error in
      if let theError = error {
        print(theError.localizedDescription)
      }
    }
  }

  fileprivate func notifyConfirmHome(id: String) {

    let homePlaceId = UserDefaults.standard.string(forKey: homePlaceIdKey)

    // If we've previoiusly stored a home place id, whether or not is was home, that matches 
    // the current arrival home id, bail.
    if let homePlaceId = homePlaceId, homePlaceId == id {
      return
    }

    let content = UNMutableNotificationContent()
    content.title = "Home identified!"
    content.body = "DeLaCasa thinks you're home now. Is that correct?"
    content.sound = UNNotificationSound.default()
    content.userInfo = [homePlaceIdUserInfoKey: id]
    content.categoryIdentifier = confirmHomeNotificationsCategoryId

    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.5, repeats: false)

    let request = UNNotificationRequest(identifier: "confirmHome", content: content, trigger: trigger)

    let center = UNUserNotificationCenter.current()
    center.add(request) { error in
      if let theError = error {
        print(theError.localizedDescription)
      }
    }

  }
}

extension Notifier: UNUserNotificationCenterDelegate {
  func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    switch response.notification.request.content.categoryIdentifier {
    case reminderNotificationsCategoryId:
      handleReminderNotificationResponse(response, withCompletionHandler: completionHandler)
    case confirmHomeNotificationsCategoryId:
      handleConfirmHomeNotificationResponse(response, withCompletionHandler: completionHandler)
    default:
      completionHandler()
    }
  }

  func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    if let homeId = notification.request.content.userInfo[homePlaceIdUserInfoKey] as? String, notification.request.content.categoryIdentifier == confirmHomeNotificationsCategoryId {
      askIfHome(id: homeId)
      completionHandler([])
    } else {
      completionHandler(UNNotificationPresentationOptions.alert)
    }
  }
}

extension Notifier {

  func handleReminderNotificationResponse(_ response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    if let reminderId = response.notification.request.content.userInfo[reminderIdUserInfoKey] as? String,
      let reminder = realm.object(ofType: Reminder.self, forPrimaryKey: reminderId),
      ReminderAction(rawValue: response.actionIdentifier) != .snooze && response.actionIdentifier != UNNotificationDefaultActionIdentifier
    {
      try! realm.write {
        reminder.state = .complete
      }
    }
    completionHandler()
  }

  func handleConfirmHomeNotificationResponse(_ response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {

    defer {
      completionHandler()
    }

    let userInfo = response.notification.request.content.userInfo

    guard let placeId = userInfo[homePlaceIdUserInfoKey] as? String else { return }

    if response.actionIdentifier == UNNotificationDefaultActionIdentifier {
      askIfHome(id: placeId)
      return
    }
  }

  func askIfHome(id: String) {
    let alertController = UIAlertController(title: "Home identified!", message: "DeLaCasa thinks you're home now. Is that correct?", preferredStyle: .alert)
    alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
    alertController.addAction(UIAlertAction(title: "No", style: .default, handler: {_ in
      self.explainLearning()
    }))
    UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
  }

  func explainLearning() {
    let alertController = UIAlertController(title: nil, message: "DeLaCasa will continue to learn your behavior and adjust your home location as it has more data.", preferredStyle: .alert)
    alertController.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
    UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
  }
}
