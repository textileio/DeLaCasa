![Logo](https://s3.amazonaws.com/set-static-assets/logos/readme.png)

# Introduction

Set is an iOS framework that delivers on-device data processing and machine learning through simple to use APIs. It helps mobile developers wrangle data from the many streams available on devices. It is not adtech, and doesn't harvest or sell your user’s data. It's a system for making your technology better, stronger, and more useful.

Current releases of the SDK focus on location, place, and time; Where is the user, what kind of place is it, how long will they be there, and where will they go next?

# Getting Started

### Requirements

- iOS 10.2+
- Xcode 8.0

### Installation with CocoaPods

```ruby
pod 'SetSDK'
```

### See the Official Docs

For a complete guide on client setup, SDK installation, and API reference, see the [SetSDK documentation](https://docs.set.gl).

# What you can build now

Here's a few examples of how you can make use of Set today:

1. Use Set to handle always-on location updates without battery drain.
2. Subscribe to notifications of a user arriving and departing locations with no need to specify places of interest. Set automatically learns all significant places.
3. When the user is about to depart on a trip, get a prediction where the destination will be (see [getDestination](https://docs.set.gl/api-reference/methods.html#get-destination)).
4. Get a prediction of how long the user will be dwelling in the place they are currently located using [getDwellTimeDuration](https://docs.set.gl/api-reference/methods.html#get-dwell-time-duration).
5. Use Set's [Context-based API](https://docs.set.gl/api-reference/methods.html#context-api) which allows you to easily subscribe to occurrences of specific conditions that you configure in a [Context](https://docs.set.gl/api-reference/types.html#context).

### Example: Get next destination on departure

```swift
SetSDK.instance.onDeparture(from: .home) { departure in
	let destinations = SetSDK.instance.getDestination(from: departure.location, atTime: departure.date)
	if let destination = destinations?.first {
		let label = destination.label ?? "your destination"
		let arrivalTime = destination.bounds.rawBounds.end
		let minutesUntilArrival = Int(Date().timeIntervalSince(arrivalTime) / 60)
	}
}
```
### Example: Use the Context API

```swift
let onArrivalHomeContext = Context(
	id: "onArrivalHome",
	notificationTiming: .after(delay: .fifteenMinutes),
	eventAction: .enter,
	eventState: .place(type: .home)
)

SetSDK.instance.register(context: onArrivalHomeContext) {
	// Do something now that we know the
	// user has been home for 15 minutes
}
```

# Coming Soon

We're hard at work adding new features and capabilities to Set. Some of these include:

1. Access the named significant places identified by Set (e.g. Home, Work). The user never needs to manually enter information.
2. At any moment in time, get the user's next most likely place or behavior.
3. Expanding the Context API to support predictions of arrival time, dwell time, and departure time.

If you're interested in beta testing pre-release versions of these features, please get in touch!

# Get in touch

* If you **need help**, shoot us a message at support@set.gl or ping us on our [Slack community](https://now-examples-slackin-cylgxxkwvl.now.sh/).
* If **found a bug**, or **have a feature request**, add an Issue on the [SetSDK Repo](https://gitlab.com/weareset/SetSDK/issues).
* Follow us on Twitter for product updates: [@everyset](https://twitter.com/everyset)

# Resources

* [Documentation](https://gitlab.com/weareset/SetSDK)
* [Website](https://www.set.gl/)
* [Blog](https://blog.set.gl/)
* Follow us on Twitter: [@everyset](https://twitter.com/everyset)
