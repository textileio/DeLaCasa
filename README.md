[![DeLaCasa](https://s3.amazonaws.com/delacasa/banners-badge.001.jpeg)](https://itunes.apple.com/us/app/de-la-casa/id1225353662?ls=1&mt=8)

### De La Casa

De La Casa is an AI powered reminders app.

Have you ever sat in the car and said, "Hey, remind me when we get home...". De
La Casa solves that for you, but helping you create reminders attached to your
behaviors around home.

It's simple, drop yourself a reminder, "Take out the trash bins". Then choose the
behavior you want to trigger the reminder. De La Casa currently offers three basic
triggers,

* Next time you are on your way home
* Next time you arrive home
* Next time you depart home

[![DeLaCasa](https://s3.amazonaws.com/delacasa/overview-banner.jpeg)](https://itunes.apple.com/us/app/de-la-casa/id1225353662?ls=1&mt=8)

### How it works

De La Casa is an experiment in context intelligence. It will spend the first day 
learning where your home is. The first notification you receive will let 
you know that the app thinks it has figured it out and is just asking you to confirm.

Next, the app just waits for events to happen around your home in order to trigger 
any reminder you setup. 

#### Want more?

We have a long list of behaviors to include and features to add, but wanted to 
start simple. If you love it and wish something was in there, shoot us a message 
on [twitter](http://twitter.com/everyset) or through our website. 

### Developers 

De La Casa uses the [SetSDK](https://www.set.gl) to embed context intelligence into the app.

If you are a developer, feel free to fork and modify this app. Or get in touch if you
want to build something entirely new. All the code can be used as an example of 
how to build an app using the SetSDK. However, there should be no need to run the 
_home verification_ step in your own app, we are simply validating our internal 
models for release.